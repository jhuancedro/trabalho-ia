//
// Created by jhcedro on 28/05/17.
//

#ifndef TRABALHO_IA_NRAINHAS_H
#define TRABALHO_IA_NRAINHAS_H

#include "Problema.h"
#include "Tabuleiro.h"

using namespace std;

string auxFormat1(string s, int i){
    char buffer [s.size() + (int) floor(log10 (abs (i))) + 1];
    sprintf(buffer, s.c_str(), i);
    return string(buffer);
}

class NRainhas1 : public Problema{
private:
    uint n;
public:
    NRainhas1(uint n) : Problema(auxFormat1("NRainhas1 (n = %d)", n)) {
        this->n = n;
    }

    Estado *getEstadoInicial() {
        return new Tabuleiro(n, 0);
    }

    int heuristica(Estado *estado) {
        Tabuleiro* tabuleiro = (Tabuleiro*) estado;
//        return tabuleiro->qdtPosicoesIndisponiveis() - 4*n*tabuleiro->getNumRainhas();
//        return tabuleiro->qdtPosicoesIndisponiveis() - n*tabuleiro->getNumRainhas();
        return (n-tabuleiro->getNumRainhas()) + tabuleiro->qdtPosicoesIndisponiveis() / (2*n);
    }

    /* Efetua a op-esima operacao (Estrategia de controle: cima->baixo, esquerda->direita)
     * retorna NULL, caso a operacao nao tenha sido efetuada */
    Estado* operacao(Estado* estado, int op) {
        Tabuleiro* tabuleiro = (Tabuleiro*) (estado->copia(estado->getProfundidade()+1));

        int cont = 0;
        for (uint i = 0; i < n ; i++){
            if(!tabuleiro->linhaOcupada(i)){
                if(i > tabuleiro->getNumRainhas())
                    return NULL;
                for (uint j = 0; j < n; j++){
                    if(tabuleiro->posicaoVazia(i, j)){
                        if(cont == op){
                            tabuleiro->inserirRainha(i, j);
//                            if(tabuleiro->qdtPosicoesIndisponiveis() >= 4*n*tabuleiro->getNumRainhas())
                                tabuleiro->setCusto(1);
//                             cout << tabuleiro->getCusto() << endl;
//                             else
//                                tabuleiro->setCusto(tabuleiro->qdtPosicoesIndisponiveis() - n*tabuleiro->getNumRainhas());
//                            tabuleiro->setHeuristica(tabuleiro->qdtPosicoesIndisponiveis() - 4*n*tabuleiro->getNumRainhas());
//                            cout << op << endl;
//                            tabuleiro->imprime();
                            return tabuleiro;
                        }
                        else cont++;
                    }
                }
            }
        }

        delete tabuleiro;
        return NULL;
    }

    /* Dois tabuleiros sao estados equivalentes se possuem rainhas nas mesmas posicoes */
    bool estadosEquivalentes(Estado *estadoA, Estado *estadoB) {
        Tabuleiro* A = (Tabuleiro *) estadoA;
        Tabuleiro* B = (Tabuleiro *) estadoB;

        if (A->getTamTabuleiro() != B->getTamTabuleiro() ||
            A->getNumRainhas() != B->getNumRainhas())
            return false;

        for( A->posicoes->itInicio(); !A->posicoes->itEhFim(); A->posicoes->itProx()) {
            if (!B->possuiRainha(A->posicoes->getIt()->i, A->posicoes->getIt()->j))
                return false;
        }

        return true;
    }

    bool estadoEhSolucao(Estado *estado) {
        return ((Tabuleiro*) estado)->getNumRainhas() == n;
    }

    void deleteEstado(Estado *estado) {
        delete  (Tabuleiro *) estado;
    }

    virtual ~NRainhas1() { }
};

#endif //TRABALHO_IA_NRAINHAS_H