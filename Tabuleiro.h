//
// Created by jhcedro on 08/05/17.
//

#ifndef TRABALHO_IA_TABULEIRO_H
#define TRABALHO_IA_TABULEIRO_H

#include <iostream>
#include "Pilha.h"
#include "Estado.h"

#define VAZIA 0
#define OCUPADA 1

//typedef short sint;
typedef unsigned int uint;

using namespace std;

typedef struct Posicao{
    uint i, j;
}Posicao;

class Tabuleiro : public Estado{
private:
    uint n, numRainhas;
    uint *linhas, *colunas, *diagonais1, *diagonais2;
    bool permitirColisoes;

    void ocuparPosicao(uint i, uint j);
    void liberarPosicao(uint i, uint j);

public:
    // Posicoes do tabuleiro ehm que ha rainha
    Pilha<Posicao*>* posicoes;

    Tabuleiro(uint n, uint produndidade, bool permitirColisoes = true);

    uint getTamTabuleiro();
    uint getNumRainhas();

    uint posDiagonal1(uint i, uint j);
    uint posDiagonal2(uint i, uint j);

    bool posicaoVazia(uint i, uint j);
    bool posicaoDisponivel(uint i, uint j);
    bool linhaOcupada(uint i);
    bool inserirRainha(uint i, uint j);
    bool moverRainha(uint rainha, uint direcao);
    bool possivelMoverRainha(uint rainha, uint direcao);
    bool possuiRainha(uint i, uint j);
    uint contarColisoes();
    bool haColisao();

    static Tabuleiro* tabuleiroAleatorio(uint n);

    void imprimeVetor(uint* vetor, uint tam);
    void imprimeInfo();
    void imprime();

    uint qdtPosicoesIndisponiveis();

    Tabuleiro* copia(uint profundidade);

    ~Tabuleiro();
};

#endif //TRABALHO_IA_TABULEIRO_H
