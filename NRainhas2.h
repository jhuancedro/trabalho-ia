//
// Created by jhcedro on 10/07/17.
//

#ifndef TRABALHO_IA_NRAINHAS2_H
#define TRABALHO_IA_NRAINHAS2_H

#include <iostream>
#include "Problema.h"
#include "Tabuleiro.h"

using namespace std;

string auxFormat(string s, int i){
    char buffer [s.size() + (int) floor(log10 (abs (i))) + 1];
    sprintf(buffer, s.c_str(), i);
    return string(buffer);
}

class NRainhas2 : public Problema{
private:
    uint n;
    Estado* estadoInicial;
public:
    NRainhas2(uint n, Tabuleiro* inicial = NULL) : Problema(auxFormat("NRainhas2 (n = %d)", n)) {
        this->n = n;
//        estadoInicial = (inicial==NULL) ? tabuleiroAleatorio() : inicial;
        if (inicial != NULL)
            estadoInicial = inicial;
        else {
//            estadoInicial = Tabuleiro::tabuleiroAleatorio(n);
//            tabuleiroAleatorio(n);
//            estadoInicial->imprime();
            estadoInicial = Tabuleiro::tabuleiroAleatorio(n);
        }
    }

    Estado *getEstadoInicial() {
//        return estadoInicial;
        return Tabuleiro::tabuleiroAleatorio(n);
//        return estadoInicial;
    }

    void tabuleiroAleatorio(uint n) {
        Tabuleiro* T = new Tabuleiro(n, 0);

        // Preenche com n Rainhas em posicoes aleatorias
        for (int k = 0; k < n; ++k) {
            uint i, j;
            do{
                i = rand() % n;
                j = rand() % n;
            }while (!T->posicaoDisponivel(i, j));

            T->inserirRainha(i, j);
//        imprime();
        }
        estadoInicial = T;

//        return T;
    }

    int heuristica(Estado *estado){
        return ((Tabuleiro*) estado)->contarColisoes();
    }

    /* Efetua a op-esima operacao (Estrategia de controle: cima->baixo, esquerda->direita)
     * n*8 operacoes disponiveis
     * retorna NULL, caso a operacao nao tenha sido efetuada */
    Estado* operacao(Estado* estado, int op) {
        Tabuleiro* tabuleiro = (Tabuleiro*) estado->copia(estado->getProfundidade()+1);
//        tabuleiro->imprime();
        int cont = 0;
        for (uint k = 0; k < n*8 ; k++) {
//            cout << "Rainha: " << k / 8 << "\t direcao: " << k%8 << endl;
            if (tabuleiro->possivelMoverRainha(k / 8, k % 8))
                if (cont == op) {
//                    cout << "operacao: " << op << endl;
                    tabuleiro->moverRainha(k / 8, k % 8);
                    tabuleiro->setCusto(tabuleiro->getCusto() + 1);
//                    tabuleiro->imprime();
//                    tabuleiro->imprimeInfo();
//                    cout << "colisoes: " << tabuleiro->contarColisoes() << endl;
                    return tabuleiro;
                }
                else cont++;
        }

        delete tabuleiro;
        return NULL;
    }

    /* Dois tabuleiros sao estados equivalentes se possuem rainhas nas mesmas posicoes */
    bool estadosEquivalentes(Estado *estadoA, Estado *estadoB) {
        Tabuleiro* A = (Tabuleiro *) estadoA;
        Tabuleiro* B = (Tabuleiro *) estadoB;

//        cout<< "checar estados eq.: " << endl;
//        estadoA->imprime();
//        estadoB->imprime();

        if (A->getTamTabuleiro() != B->getTamTabuleiro() ||
            A->getNumRainhas() != B->getNumRainhas())
            return false;

        for( A->posicoes->itInicio(); !A->posicoes->itEhFim(); A->posicoes->itProx()) {
            if (!B->possuiRainha(A->posicoes->getIt()->i, A->posicoes->getIt()->j))
                return false;
        }

        return true;
    }

    bool estadoEhSolucao(Estado *estado) {
        return ((Tabuleiro*) estado)->getNumRainhas() == n
               && !((Tabuleiro*) estado)->haColisao();
    }

    void deleteEstado(Estado *estado) {
        delete  (Tabuleiro *) estado;
    }

    virtual ~NRainhas2() { }
};


#endif //TRABALHO_IA_NRAINHAS2_H
