//
// Created by jhcedro on 09/06/17.
//
#ifndef TRABALHO_IA_SEQUENCIA_H
#define TRABALHO_IA_SEQUENCIA_H

#include "No.h"

typedef unsigned int uint;

template <class T>
class Pilha;

/* Estrutura abstrata para uma sequencia de objetos (pilha, fila, lista...). */
template<class T>
class Sequencia {
protected:
    No<T> *inicio;
    No<T> *it;
    Pilha<No<T>*>* itPilha;
    uint tam;
public:
    Sequencia(){
        inicio = NULL;
        it = NULL;
        tam = 0;
        itPilha = NULL;
    }

    T getInicio() const {
        return inicio->getInfo();
    }

    unsigned int getTam(){
        return tam;
    }

    virtual void inserir(T obj, int valor = 0) = 0;
    virtual T remover() = 0;

    /*  iterador  */

    // Define o iterador como o inicio da sequencia
    void itInicio(){
        it = inicio;
    }

    // Define o iterador como o proximo da sequencia
    void itProx(){
        it = it->getProx();
    }

    // Verifica o iterador jah percorreu toda sequencia
    bool itEhFim(){
        return it == NULL;
    }

    // Define iterador como NULL
    void itNULL(){
        it = NULL;
    }

    //Empilha o iterador atual
    void pushIt(){
        if (itPilha == NULL){
            itPilha = new Pilha<No<T>*>();
        }
        itPilha->inserir(it);
    }

    //Desempilha um iterador
    void popIt(){
        if (itPilha == NULL){
            itPilha = new Pilha<No<T>*>();
        }
        it = itPilha->remover();
    }

    // Retorna objeto indicado pelo iterador
    T getIt(){
        /* Usuario nao enxerga o uso de No */
        return it->getInfo();
    }

    // verifica se sequencia eh vazia
    bool ehVazia(){
        return inicio == NULL;
    }
};

#endif //TRABALHO_IA_SEQUENCIA_H
