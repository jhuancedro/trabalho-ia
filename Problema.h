//
// Created by jhcedro on 28/05/17.
//

#ifndef TRABALHO_IA_PROBLEMA_H
#define TRABALHO_IA_PROBLEMA_H

#include <string>
#include "Estado.h"
#include <cmath>

using namespace std;
typedef unsigned int uint;

class Problema {
protected:
    string nome;
public:
    Problema(string nome) : nome(nome) {}

    virtual string getNome(){
        return nome;
    }

    virtual void setNome(string nome) {
        this->nome = nome;
    }

    virtual Estado* getEstadoInicial() = 0;

    /* Efetua a op-esima operacao
    * retorna NULL, caso a operacao nao tenha sido efetuada */
    virtual Estado* operacao(Estado* estado, int op) = 0;

    /* Verifica se, para o presente problema, dois estados sao equivalentes */
    virtual bool estadosEquivalentes(Estado* estadoA, Estado* estadoB) = 0;

    /* Verifica se um estado eh solucao do problema
     * Retorna false, caso o estado nao exista */
    virtual bool estadoEhSolucao(Estado* estado) = 0;

    // Deleta apropriadamente um estado do problema
    virtual void deleteEstado(Estado* estado) = 0;

    // Heuristica para um dado estado
    virtual int heuristica(Estado*  estado) = 0;

//    void imprimeEstado(){}
//    void imprimeInfos(){}
};

#endif //TRABALHO_IA_PROBLEMA_H
