//
// Created by jhcedro on 03/06/17.
//

#ifndef TRABALHO_IA_ESTADO_H
#define TRABALHO_IA_ESTADO_H

#include <stdlib.h>

typedef unsigned int uint;

class Estado {
private:
    uint custo, profundidade;
public:
    Estado(uint profundidade) {
        this->profundidade = profundidade;
    };

    /*cria uma copia do estado pai que o gerou */
    virtual Estado* copia(uint profundidade) = 0;
    virtual void imprime() = 0;

    virtual ~Estado() {};

    void setCusto(uint valor){
        custo = valor;
    };

    uint getCusto(){
        return  custo;
    };

    uint setProfundidade(uint x){
        profundidade = x;
    }

    uint getProfundidade(){
        return profundidade;
    }
};


#endif //TRABALHO_IA_ESTADO_H
