//
// Created by jhcedro on 09/06/17.
//

#ifndef TRABALHO_IA_NO_H
#define TRABALHO_IA_NO_H

#include <cstdlib>
typedef unsigned int uint;

/* Elemento de uma estrutura sequencial (pilha, fila, lista)*/
template<class T>
class No{
private:
    No<T>* prox;
    T info;
public:
    No(const T &info){
        this->info = info;
        prox = NULL;
    }

    No <T> *getProx() const {
        return prox;
    }

    void setProx(No <T> *prox) {
        No::prox = prox;
    }

    const T &getInfo() const {
        return info;
    }

    ~No(){}
};

template <class T>
class NoLista : public No<T>{
private:
    int peso;
public:
    NoLista(T obj, int peso) : No<T>(obj){
            this->peso = peso;
    }

    int getPeso(){
        return peso;
    }
};


#endif //TRABALHO_IA_NO_H
