#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Sequencia.h"

typedef unsigned int uint;

using namespace std;

template<class T>
class Lista : public Sequencia<T>{
private:
    No<T>* fim;
public:
    typedef Sequencia<T> S;

    /* inicializa uma Lista vazia*/
    Lista<T>() : Sequencia<T>(){ };

    /* inicializa uma Lista a partir de um vetor*/
    Lista<T>(uint n, T* v) : Sequencia<T>(){
        S::itNULL();
        for (uint i = 0; i < n; ++i) {
            S::inserir( v[i] );
        }
    };

    /* inicializa uma Lista a partir de outra Lista*/
    Lista<T>(Lista<T>* lista) : Sequencia<T>(){
        S::itNULL();
        for (lista->itInicio(); !lista->itEhFim(); lista->itProx()) {
            S::inserir( lista->getIt() );
        }
    };

    /* Insere objeto no apos o iterador da Lista.
     * Caso o iterador seja NULL, a insersao ocorre no inicio. */
    void inserir(T obj, int peso){

        NoLista<T> *novo = new NoLista<T>(obj, peso);

        // Se lista eh vazia
        if(S::ehVazia()) {
            S::inicio = fim = novo;
        }

        // Inserir no inicio
        else{
            verificaLugarIt(peso);

            if (S::it == NULL){
                novo->setProx(S::inicio);
                S::inicio = novo;
            }

                // Inserir no meio ou fim
            else{
                novo->setProx( S::it->getProx() );
                S::it->setProx( novo );
                if (S::it == fim)
                    fim = novo;
            }
        }

        S::tam++;
    };

    //menor custo fica no final
    void verificaLugarIt(uint peso) {
        if (((NoLista<T>*) S::inicio)->getPeso() > peso){
            S::it = NULL;
        } else{
            for (S::itInicio(); !S::itEhFim(); S::itProx()) {
                if(S::it == fim)
                    return;
                else if(((NoLista<T>*) S::it->getProx())->getPeso() > peso){
                    return;
                }
            }
        }
    }

//    void inserirOrdenado(T obj, uint custo){
//        S::it = verificaLugar(custo);
//        inserir(obj);
//    };

    T remover(){
        S::it = S::inicio;
        return removerIt();
    }

    /* Remove o objeto referenciado pelo iterador da Lista.
     * Iterador passa referenciar o proximo da Lista */
    T removerIt(){
        T obj = (T) NULL;
        if (S::it != NULL) {
            obj = S::it->getInfo();

            No<T> *del = S::it;
            if (del == S::inicio)
                S::inicio = del->getProx();
            else {
                for (S::itInicio(); S::it->getProx() != del; S::itProx());
                S::it->setProx(del->getProx());
            }

            // Iterador passa para o proximo da lista
            S::it = del->getProx();

//            cout << "Peso: " << ((NoLista<T>*) del)->getPeso() << endl;
            delete del;
            S::tam--;
        }

        return obj;
    }

    /* Remove o i-esimo objeto da Lista
     * retorna NULL, caso nao exista*/
    T pop(uint i){
        No<T> *del = NULL;

        if(i == 0){
            del = S::inicio;
            S::inicio = del->getProx();
        } else {
            uint cont = 1;
            for (S::itInicio(); S::it->getProx() != fim, cont == i; S::itProx(), cont++);
            if (cont == i) {
                del = S::it->getProx();
                S::it->setProx(del->getProx());
            }
        }

        if(del != NULL){
            T obj = del->getInfo();
            delete del;
            S::tam--;

            return obj;
        }

        return NULL;
    }

    /* Define o iterador como o fim da Lista */
    void itFim(){   S::it = fim;    }

    Lista<T>* copia(){
        return new Lista<T>(this);
    }

    ~Lista(){
        while (S::inicio != NULL){
            this->remover();
        }
    };
};


#endif // LISTA_H_INCLUDED
