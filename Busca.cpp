#include <cmath>
#include <ctime>
#include "Busca.h"
#include "Lista.h"
#include "Fila.h"

Busca::Busca(Problema *problema) : problema(problema) {
    estatisticas = new Estatisticas;
}

Busca::~Busca() {
    delete estatisticas;
}

void Busca::zerarEstatisticas() {
    estatisticas->qtdEstadosExpandidos = 0;
    estatisticas->qtdEstadosVisitados = 0;
    estatisticas->fatorRamificacao = 0;
    estatisticas->profundidade = 0;
    estatisticas->custoDaSolucao = 0;
    estatisticas->clocks = 0;
}

/* Verifica se uma Sequencia (estados) possui um estado equivalente a (estado)*/
bool Busca::possuiEstadoEquiv(Sequencia<Estado *> *estados, Estado *estado) {
    for(estados->itInicio(); !estados->itEhFim(); estados->itProx()){

//        cout << "*";    estados->getIt()->imprime();
//        cout << "*";    estado->imprime();

        if(problema->estadosEquivalentes(estados->getIt(), estado))
            return true;
    }

    return false;
}

Estado* Busca::backtracking() {
    zerarEstatisticas();
    estatisticas->algoritmo = BACKTRACKING;
    estatisticas->problema = problema->getNome();
    Estado* solucao = NULL;
    Pilha<Estado*> *estadosA = new Pilha<Estado*>();
    Pilha<Estado*> *estadosF = new Pilha<Estado*>();
    Pilha<uint> *operacoes = new Pilha<uint>();

    estatisticas->clocks = clock();
    estadosA->inserir(problema->getEstadoInicial());
    operacoes->inserir(0);
    estadosA->getTopo()->imprime();
//    estadosA->itInicio();
//    estadosA->getIt()->imprime();
    estatisticas->qtdEstadosExpandidos++;

    while(!estadosA->ehVazia() && !problema->estadoEhSolucao(estadosA->getTopo())){
        if(operacoes->getTopo() == 0) // primeira vez que visita o estado
            estatisticas->qtdEstadosVisitados++;

        Estado* novoEstado = problema->operacao(estadosA->getTopo(), operacoes->getTopo());
        // Nao ha mais estadosA a serem gerados
        if(novoEstado != NULL) {
            // Estado gerado nao eh repetido
            if(!possuiEstadoEquiv(estadosA, novoEstado) &&
                !possuiEstadoEquiv(estadosF, novoEstado)){
                estadosA->inserir(novoEstado);
                operacoes->inserir(0);
                estatisticas->qtdEstadosExpandidos++;
            } else {
                operacoes->inserir(operacoes->remover() + 1);
                problema->deleteEstado(novoEstado);
//                cout << "*Estado eq." << endl;
            }
        }
        else{
            //Nao ha solucoes para este estado
//            problema->deleteEstado(estadosA->remover());
            estadosF->inserir(estadosA->remover());
            operacoes->remover();
//            cout << "*BT" << endl;

            //Incrementa o operador do estado anterior
            if(!operacoes->ehVazia()) {
                operacoes->inserir(operacoes->remover() + 1);
            }

        }
    }
    estatisticas->clocks = clock() - estatisticas->clocks;

    if(!estadosA->ehVazia()) {
        solucao = estadosA->remover();
        estatisticas->qtdEstadosVisitados++;
    }

    estatisticas->profundidade = estadosA->getTam();

    while (!estadosA->ehVazia()) {
        problema->deleteEstado(estadosA->remover());
    }
    while (!estadosF->ehVazia()) {
        problema->deleteEstado(estadosF->remover());
    }

    estatisticas->custoDaSolucao = (solucao == NULL) ? (uint) -1 : solucao->getCusto();
    estatisticas->fatorRamificacao = (float) estatisticas->qtdEstadosExpandidos / estatisticas->qtdEstadosVisitados;

    return solucao;
}

/* Abstracao das buscas em largura, profundidade, ordenada, gulosa e A* */
Estado* Busca::auxAlgoritmosBusca(int algoritmo) {
    zerarEstatisticas();
    estatisticas->algoritmo = (uint) algoritmo;
    estatisticas->problema = problema->getNome();
    Estado* solucao = NULL;

    // Estados abertos
    Sequencia<Estado*> *estadosA;
    Sequencia<Estado*> *estadosF = new Pilha<Estado*>();


    switch (algoritmo){
        case PROFUNDIDADE:  estadosA = new Pilha<Estado*>();  break;
        case LARGURA:       estadosA = new Fila<Estado*>();   break;
        default:            estadosA = new Lista<Estado*>();
    }

    int valor;

    estatisticas->clocks = clock();

    Estado* estadoInicial = problema->getEstadoInicial();
    switch (algoritmo){
        case GULOSA:        valor = problema->heuristica(estadoInicial); break;
        case A_ESTRELA:     valor = problema->heuristica(estadoInicial) + estadoInicial->getCusto(); break;
        default:            valor = estadoInicial->getCusto(); break;
    }
    estadosA->inserir(estadoInicial, valor);
    estatisticas->qtdEstadosExpandidos++;

    estadosA->getInicio()->imprime();

    int contOperacao;
    while(!estadosA->ehVazia() && !problema->estadoEhSolucao(estadosA->getInicio())){
        estatisticas->qtdEstadosVisitados++;;
        contOperacao = 0;
        Estado* atual = estadosA->remover();
        estadosF->inserir(atual);

        // Abre todos os filhos
        Estado* novoEstado = problema->operacao(atual, contOperacao);
        while(novoEstado != NULL) {
            switch (algoritmo) {
                case GULOSA:
                    valor = (uint) problema->heuristica(novoEstado);
                    break;
                case A_ESTRELA:
                    valor = problema->heuristica(novoEstado) + novoEstado->getCusto();
                    break;
                default:
                    valor = novoEstado->getCusto();
                    break;
            }
            // Verifica exixtencia de estado aberto equivalente ao novo estado
            if (possuiEstadoEquiv(estadosA, novoEstado)){
                if (algoritmo != A_ESTRELA || (algoritmo == A_ESTRELA
                    && valor > (estadosA->getIt()->getCusto() + problema->heuristica(estadosA->getIt())))) {
                    problema->deleteEstado(novoEstado);
//                cout << "*Estado eq." << endl;
                } else {
                    estadosA->inserir(novoEstado, valor);
                    estatisticas->qtdEstadosExpandidos++;
                }
            }
            else {
                 if (!possuiEstadoEquiv(estadosF, novoEstado)) {
                     estadosA->inserir(novoEstado, valor);
                     estatisticas->qtdEstadosExpandidos++;
                 }else
                     problema->deleteEstado(novoEstado);
            }
            novoEstado = problema->operacao(atual, ++contOperacao);
        }
//        problema->deleteEstado(atual);
    }
    estatisticas->clocks = clock() - estatisticas->clocks;

    if(!estadosA->ehVazia())
        solucao = estadosA->remover();

    cout << "Estados abertos: " << estadosA->getTam() << endl;

    //desalocao das pilhas
    while (!estadosA->ehVazia()) {
        problema->deleteEstado(estadosA->remover());
    }

    while  (!estadosF->ehVazia())
        problema->deleteEstado(estadosF->remover());

    cout << "numEstadosVizitados: " <<  estatisticas->qtdEstadosVisitados << endl;

    estatisticas->profundidade = (solucao == NULL) ? (uint) -1 : solucao->getProfundidade();
    estatisticas->custoDaSolucao = (solucao == NULL) ? (uint) -1 : solucao->getCusto();
    estatisticas->fatorRamificacao = (float) estatisticas->qtdEstadosExpandidos / estatisticas->qtdEstadosVisitados;

    return solucao;
}

Estado* Busca::profundidade() { return auxAlgoritmosBusca(PROFUNDIDADE); }
Estado* Busca::largura()      { return auxAlgoritmosBusca(LARGURA);      }
Estado* Busca::ordenada()     { return auxAlgoritmosBusca(ORDENADA);     }
Estado* Busca::gulosa()       { return auxAlgoritmosBusca(GULOSA);       }
Estado* Busca::aEstrela()     { return auxAlgoritmosBusca(A_ESTRELA);    }

/* OBS: Quantidade de estados visitados do IDA* conta com repeticao
 * de estados devido aa mudanca de patamar
 * */
Estado* Busca::idaEstrela() {
    zerarEstatisticas();
    estatisticas->algoritmo = IDA_ESTRELA;
    estatisticas->problema = problema->getNome();
    Estado* solucao = NULL;
    Pilha<Estado*> *estadosA = new Pilha<Estado*>();
    Pilha<Estado*> *estadosF = new Pilha<Estado*>();
    Pilha<uint> *operacoes = new Pilha<uint>();
    int patamar;
    int menorVal = (int) HUGE_VAL;
    int valor = 0;

    estatisticas->clocks = clock();
    estadosA->inserir(problema->getEstadoInicial());
//    estadosA->itInicio();
//    estadosA->getIt()->imprime();
//    patamar = problema->heuristica(estadosA->getTopo()) + 1;
    patamar = problema->heuristica(estadosA->getTopo());
    estatisticas->qtdEstadosExpandidos++;

    estadosA->getTopo()->setCusto(0);
    operacoes->inserir(0);

    estadosA->getInicio()->imprime();

    bool podado = true;

    while (patamar != 0 && podado){
        podado = false;
        cout << patamar << endl;

        while (!estadosA->ehVazia() && !problema->estadoEhSolucao(estadosA->getTopo())) {
            if(operacoes->getTopo() == 0) // primeira vez que visita o estado
                estatisticas->qtdEstadosVisitados++;

            Estado *novoEstado = problema->operacao(estadosA->getTopo(), operacoes->getTopo());
            // Nao ha mais estadosA a serem gerados
            if (novoEstado != NULL) {
//            // Estado gerado nao eh repetido
//            if(!possuiEstadoEquiv(estadosA, novoEstado)){
                valor = problema->heuristica(novoEstado) + novoEstado->getCusto();
                if ((!possuiEstadoEquiv(estadosA, novoEstado) &&
                     !possuiEstadoEquiv(estadosF, novoEstado)) &&
                     valor <= patamar) {
//                    cout << "valor: " << valor << endl;
                    estadosA->inserir(novoEstado, valor);
                    operacoes->inserir(0);
                    estatisticas->qtdEstadosExpandidos++;
                } else {
                    if(valor > patamar && valor < menorVal) {
                        menorVal = valor;
                    }

                    operacoes->inserir(operacoes->remover() + 1);
                    problema->deleteEstado(novoEstado);
                    podado = true;
                }
            } else {
                //Nao ha solucoes para este estado
//                problema->deleteEstado(estadosA->remover());
                estadosF->inserir(estadosA->remover());
                operacoes->remover();
//            cout << "*BT" << endl;

                //Incrementa o operador do estado anterior
                if (!operacoes->ehVazia()) {
                    operacoes->inserir(operacoes->remover() + 1);
                }
            }
        }
//        if(podado)
//            patamar = menorVal;
//        else
//            patamar = 0;

        if(patamar == menorVal)
            patamar = 0;
        else
            patamar = menorVal;


        if (!estadosA->ehVazia()) {
            solucao = estadosA->remover();
            patamar = 0;
            estatisticas->profundidade = solucao->getProfundidade();
        }

        while (!estadosA->ehVazia()) {
            problema->deleteEstado(estadosA->remover());
        }
        while (!estadosF->ehVazia()) {
            problema->deleteEstado(estadosF->remover());
        }

        // Recomeca busca
        estadosA->inserir(problema->getEstadoInicial());
        estadosA->getTopo()->setCusto(0);
        operacoes->inserir(0);
        menorVal = (int) HUGE_VAL;
        estatisticas->qtdEstadosExpandidos++;
    }
    estatisticas->clocks = clock() - estatisticas->clocks;
    cout << "numEstadosVizitados: " <<  estatisticas->qtdEstadosVisitados << endl;

    estatisticas->custoDaSolucao = (solucao == NULL) ? (uint) -1 : solucao->getCusto();
    estatisticas->fatorRamificacao = (float) estatisticas->qtdEstadosExpandidos / estatisticas->qtdEstadosVisitados;

    return solucao;
}

Estado* Busca::executarAlgoritmo(int algoritmo) {
    switch (algoritmo){
        case BACKTRACKING:  return backtracking();
        case PROFUNDIDADE:  return profundidade();
        case LARGURA:       return largura();
        case ORDENADA:      return ordenada();
        case GULOSA:        return gulosa();
        case A_ESTRELA:     return aEstrela();
        case IDA_ESTRELA:   return idaEstrela();
        default:            return NULL;
    }
}

string Busca::nomeAlgoritmo(int algoritmo) {
    switch (algoritmo){
        case BACKTRACKING:  return "Backtracking";
        case PROFUNDIDADE:  return "Profundidade";
        case LARGURA:       return "Largura";
        case ORDENADA:      return "Ordenada";
        case GULOSA:        return "Gulosa";
        case A_ESTRELA:     return "A*";
        case IDA_ESTRELA:   return "IDA*";
        default:            return "???";
    }
}

void Busca::imprimeEstatisticas() {
    cout << "algoritmo: " << nomeAlgoritmo(estatisticas->algoritmo) << endl;
    cout << "problema: " << estatisticas->problema   << endl;
    cout << "qtdEstadosExpandidos: " << estatisticas->qtdEstadosExpandidos   << endl;
    cout << "qtdEstadosVisitados: " << estatisticas->qtdEstadosVisitados << endl;
    cout << "fatorRamificacao: " << estatisticas->fatorRamificacao   << endl;
    cout << "profundidade: " << estatisticas->profundidade   << endl;
    cout << "custoDaSolucao: " << estatisticas->custoDaSolucao   << endl;
    cout << "clocks: " << estatisticas->clocks << endl << endl;
}

/* Retorna uma cópia das estatísticas atuais */
Estatisticas *Busca::getEstatisticas() {
    Estatisticas* e = new Estatisticas;
    e->algoritmo            = estatisticas->algoritmo;
    e->problema             = estatisticas->problema;
    e->qtdEstadosVisitados  = estatisticas->qtdEstadosVisitados;
    e->qtdEstadosExpandidos = estatisticas->qtdEstadosExpandidos;
    e->fatorRamificacao     = estatisticas->fatorRamificacao;
    e->profundidade         = estatisticas->profundidade;
    e->custoDaSolucao       = estatisticas->custoDaSolucao;
    e->clocks                = estatisticas->clocks;
    return e;
}
