//
// Created by jhcedro on 08/05/17.
//

#ifndef TRABALHO_IA_PILHA_H
#define TRABALHO_IA_PILHA_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "No.h"
#include "Sequencia.h"

typedef unsigned int uint;

using namespace std;

template<class T>
class Pilha : public Sequencia<T>{
public:
    Pilha<T>() : Sequencia<T>(){};

    typedef Sequencia<T> S;

    /* Insere objeto no topo da pilha */
    void inserir(T obj, int valor = 0){
        No<T> *novo = new No<T>(obj);
        novo->setProx(S::inicio);
        S::inicio = novo;
        S::tam++;
    }

    /* Remove objeto do topo da pilha */
    T remover(){
        T obj = S::inicio->getInfo();
        No<T> *del = S::inicio;
        S::inicio = S::inicio->getProx();
        delete del;
        S::tam--;

        return obj;
    }

    T getTopo(){
        return S::inicio->getInfo();
    }



    ~Pilha(){
        while (S::inicio != NULL){
            this->remover();
        }
    };
};

#endif //TRABALHO_IA_PILHA_H
