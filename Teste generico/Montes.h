//
// Created by jhcedro on 09/06/17.
//

#ifndef TRABALHO_IA_MONTES_H
#define TRABALHO_IA_MONTES_H


#include <iostream>
#include "../Estado.h"

typedef unsigned int uint;
typedef unsigned int Monte;

using namespace std;

class Montes : public Estado{
private:
    uint n;
public:
    //FIXME: Encapsulamento comprometido em virtude de MontesIguais.estadosEquivalentes
    Monte* montes;

    Montes(uint n, uint* v, uint profundidade) : Estado(profundidade){
        this->n = n;
        montes = new Monte[n];
        for (int i = 0; i < n; ++i)
            montes[i] = v[i];
    }

    uint getNumMontes(){
        return n;
    }

    /* Retorna valor do i-esimo monte */
    uint getMonte(uint i) { return montes[i];    }

    bool transferir(uint qtd, uint i, uint j){
        if(montes[i] >= qtd){
            montes[i] -= qtd;
            montes[j] += qtd;
            return true;
        }
        return false;
    }

    Estado *copia(uint profundidade) {
        return new Montes(n, montes, profundidade);
    }

    int getHeuristica(){
        return 1;
    }

    void imprime() {
        cout << "Montes: (";
        for (int i = 0; i < n; i++)
            printf("%d %s", montes[i], (i != n-1 ? ", " : ""));
        cout << ")" << endl << endl;
    }

    ~Montes() {
        delete [] montes;
    }
};


#endif //TRABALHO_IA_MONTES_H
