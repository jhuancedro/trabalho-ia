//
// Created by jhcedro on 09/06/17.
//

#ifndef TRABALHO_IA_MONTESIGUAIS_H
#define TRABALHO_IA_MONTESIGUAIS_H


#include "../Problema.h"
#include "Montes.h"

class MontesIguais : public Problema{
private:
    uint n;
    Montes* estadoInicial_;
public:
    /* n deve ser maior que 1*/
    MontesIguais(uint n, uint* v) : Problema("Montes Iguais") {
        this-> n = n;
        estadoInicial_ = new Montes(n, v, 0);
    }

    Estado* getEstadoInicial() {
        return estadoInicial_;
    }

    virtual int heuristica(Estado *estado) {
        return 1;
    }

    Estado* operacao(Estado *estado, int op) {
        // FIXME: fazer verificacao de estados anteriores
        Montes* novoEstado = (Montes *) estado->copia(estado->getProfundidade()+1);

        uint o, d, contOp = 0, i = 0;
        while (i < n*(n-1)) {
            o = i/(n-1);
            d = i%(n-1);
            d = (d < o) ? d : d+1;

            // nenhum monte pode ficar com zero!
            if (novoEstado->getMonte(o) > novoEstado->getMonte(d)){
                if (contOp == op){
                    uint qtd = novoEstado->getMonte(d);
                    novoEstado->transferir(qtd, o, d);

//                    cout << op << endl;
//                    novoEstado->imprime();

                    return novoEstado;
                }
                contOp++;
            }
            i++;

        }

        delete novoEstado;
        novoEstado = NULL;

        return novoEstado;
    }

    /* Dois conjuntos de montes sao equivalentes se possuem a mesma combinacao de montes */
    virtual bool estadosEquivalentes(Estado *estadoA, Estado *estadoB) {
        Montes* A = (Montes *) estadoA->copia(0);
        Montes* B = (Montes *) estadoB->copia(0);

        if(A->getNumMontes() != B->getNumMontes())
            return false;

        for (uint i = 0; i < A->getNumMontes(); ++i) {
            uint j = 0;
            for ( ;j < n; j++){
                if(B->getMonte(j) == A->getMonte(i)) {
                    B->montes[j] = 0;
                    break;
                }
            }
            if (j >= n)
                return false;
        }

        return true;
    }

    bool estadoEhSolucao(Estado *estado) {
        Montes* M = (Montes *) estado;
        bool ehSolucao = true;

//        for (int i = 1; i < n && ehSolucao; i++)
//            ehSolucao = ehSolucao && M->getMonte(0) == M->getMonte(i);
        uint i = 1;
        while (i < n && ehSolucao)
            ehSolucao = ehSolucao && M->getMonte(0) == M->getMonte(i++);

        return ehSolucao;
    }

    void deleteEstado(Estado *estado) {
        delete (Montes *) estado;
    }

};

#endif //TRABALHO_IA_MONTESIGUAIS_H
