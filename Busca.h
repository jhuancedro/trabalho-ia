//
// Created by jhcedro on 28/05/17.
//

#ifndef TRABALHO_IA_BUSCA_H
#define TRABALHO_IA_BUSCA_H

#include "Problema.h"
#include "Tabuleiro.h"

//Algoritmos de busca
#define BACKTRACKING 1
#define PROFUNDIDADE 2
#define LARGURA 3
#define ORDENADA 4
#define GULOSA 5
#define A_ESTRELA 6
#define IDA_ESTRELA 7

typedef struct Estatisticas{
    uint algoritmo;
    string problema;
    uint qtdEstadosExpandidos;
    uint qtdEstadosVisitados;
    float fatorRamificacao;
    uint profundidade;
    uint custoDaSolucao;
    clock_t clocks;

} Estatisticas;

class Busca {
private:
    Problema* problema;
    Estado *auxAlgoritmosBusca(int algoritmo);
    Estatisticas* estatisticas;
    void zerarEstatisticas();
public:
    Busca(Problema *problema);

    Estado* backtracking();
    Estado* profundidade();
    Estado* largura();
    Estado* ordenada();
    Estado* gulosa();
    Estado* aEstrela();
    Estado* idaEstrela();

    bool possuiEstadoEquiv(Sequencia<Estado *> *estados, Estado *estado);

    Estado* executarAlgoritmo(int algoritmo);
    static string nomeAlgoritmo(int algoritmo);

    void imprimeEstatisticas();

    Estatisticas* getEstatisticas();

    virtual ~Busca();

};


#endif //TRABALHO_IA_BUSCA_H
