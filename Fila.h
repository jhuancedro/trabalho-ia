//
// Created by jhcedro on 08/05/17.
//

#ifndef TRABALHO_IA_FILA_H
#define TRABALHO_IA_FILA_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Sequencia.h"

template<class T>
class Fila : public Sequencia<T> {
private:
    No<T> *fim;
public:
    Fila() : Sequencia<T>(){
        fim = NULL;
    }

    typedef Sequencia<T> S;

    /* Insere objeto no fim da fila */
    void inserir(T obj, int valor = 0){
        No<T> *novo = new No<T>(obj);
        if(S::inicio == NULL)
            S::inicio = fim = novo;
        else {
            fim->setProx(novo);
            fim = novo;
        }
        S::tam++;
    }

    /* Remove objeto no inicio da fila */
    T remover(){
        T obj = S::inicio->getInfo();
        No<T> *del = S::inicio;
        S::inicio = S::inicio->getProx();
        delete del;
        S::tam--;

        return obj;
    }

    ~Fila(){
        while (S::inicio != NULL){
            this->remover();
        }
    }
};

typedef unsigned int uint;


#endif //TRABALHO_IA_FILA_H
