#include <iostream>
#include <ctime>
#include <fstream>
#include <sys/stat.h>
#include "Pilha.h"
#include "Tabuleiro.h"
#include "Problema.h"
#include "NRainhas1.h"
#include "Busca.h"
#include "Fila.h"
#include "Lista.h"
#include "Teste generico/MontesIguais.h"
#include "NRainhas2.h"

using namespace std;

#define PILHA 1
#define FILA 2
#define LISTA 3

#define NRAINHAS1 1
#define NRAINHAS2 2
#define PASTA_DESEMPENHO "Desempenho/"
/* Testa o comportamento das estruturas de sequencia implementadas */
void testeSequencia(int estrutura){
    Sequencia<int>* sequencia;
    switch (estrutura){
        case PILHA:     sequencia = new Pilha<int>();   cout << "Pilha:\n";     break;
        case FILA:      sequencia = new Fila <int>();   cout << "Fila: \n";     break;
        case LISTA:     sequencia = new Lista<int>();   cout << "Lista:\n";     break;
        default:        return;
    }


    for (int i = 0; i < 10; i++)
        sequencia->inserir(i, ((uint) rand())%100);

    sequencia->itInicio();
    for (int i = 0; i < 10; i++)
        cout << sequencia->remover() << endl;
    cout << endl;
}

void testeTabuleiro(){
    Tabuleiro* T = new Tabuleiro(3, 0);
    T->imprimeInfo();
    T->inserirRainha(0, 0);
    T->inserirRainha(1, 2);
    T->inserirRainha(2, 1);
    cout << endl;
    T->imprimeInfo();
    T->imprime();
}

/* Testa a execucao de um algoritmo de busca em um problema */
void auxTesteBusca(Problema *problema, int algoritmo) {
    Busca* busca = new Busca(problema);

    cout << problema->getNome() << " usando " << Busca::nomeAlgoritmo(algoritmo) << endl;

    Estado* solucao = busca->executarAlgoritmo(algoritmo);

    if(solucao != NULL)
        solucao->imprime();
    else
        cout << "nao ha solucao" << endl;

    busca->imprimeEstatisticas();

    delete problema;
    delete busca;
    delete solucao;
}

/* Testa a execucao de um algoritmo de busca em um problema de N-Rainhas com N = n */
void auxTesteBuscaNRainhas(int nRainhas, int algoritmo, uint n){
    Problema* problema = NULL;
    if (nRainhas == NRAINHAS1)
        problema = new NRainhas1(n);
    else
        problema = new NRainhas2(n);
    auxTesteBusca(problema, algoritmo);
}

/* Testa a execucao de um algoritmo de busca em um problema de N-Rainhas
 * com N = min ou min <= N <= max */
void testeBuscaNRainhas(int nRainhas, int algoritmo, uint min, uint max = 0){
    do{
        auxTesteBuscaNRainhas(nRainhas, algoritmo, min++);
    }while (min <= max);
}

/* Testa a execucao de um algoritmo de busca em um problema de N-Rainhas
 * com N = min ou min <= N <= max */
void testeBusca(int nRainhas, int algoritmo, uint min, uint max = 0){
    do{
        auxTesteBuscaNRainhas(nRainhas, algoritmo, min++);
    }while (min <= max);
}

/* Testa a execucao de um algoritmo de busca em um problema de N-MontesIguais com N = n */
void testeMontesIguais(int algoritmo){
    uint n = 3;
    uint v[3] = {4, 6, 2};

    // Geram uma recursao gigantesca!:
//    uint v[3] = {6, 8, 4};
//    uint v[3] = {7, 9, 5};

    MontesIguais* montesIguais = new MontesIguais(n, v);
    auxTesteBusca(montesIguais, algoritmo);
}

void testeOperacoesMontes(uint n){
    for (int i = 0; i < n*(n-1); ++i) {
        int o = i/(n-1);
        int d = i%(n-1);
        if (d < o)
            printf("%2d: %2d -> %2d\n", i, o, d);
        else
            printf("%2d: %2d -> %2d\n", i, o, d+1);
    }

}

void auxTesteDesempenho(Problema* problema, uint amostra = 1){
    Busca* busca = new Busca(problema);
    Estatisticas* medias[IDA_ESTRELA+1], *aux;
    for (int a = 0; a < amostra; ++a) {
        for (int alg = BACKTRACKING; alg <= IDA_ESTRELA; ++alg) {
            cout << "Testando desempenho: " << problema->getNome() << " usando " << busca->nomeAlgoritmo(alg) << "  amostra: " << a << endl;
            Estado *solucao = busca->executarAlgoritmo(alg);

            if (solucao != NULL) {
                solucao->imprime();
                busca->imprimeEstatisticas();
            } else
                cout << "nao ha solucao" << endl;

            aux = busca->getEstatisticas();

            if (a == 0){
                medias[alg] = new Estatisticas;
                medias[alg]->problema = aux->problema;
                medias[alg]->algoritmo = aux->algoritmo;
                medias[alg]->qtdEstadosExpandidos = 0;
                medias[alg]->qtdEstadosVisitados = 0;
                medias[alg]->fatorRamificacao = 0;
                medias[alg]->profundidade = 0;
                medias[alg]->custoDaSolucao = 0;
                medias[alg]->clocks = 0;
            }

            medias[alg]->qtdEstadosExpandidos += aux->qtdEstadosExpandidos;
            medias[alg]->qtdEstadosVisitados  += aux->qtdEstadosVisitados;
            medias[alg]->fatorRamificacao     += aux->fatorRamificacao;
            medias[alg]->profundidade         += aux->profundidade;
            medias[alg]->custoDaSolucao       += aux->custoDaSolucao;
            medias[alg]->clocks               += aux->clocks;
            delete aux;
        }
    }


    cout<<"\nSalvando desempenho"<<endl;
    string nomeArq = "desempenho" + problema->getNome() + ".csv";
    ofstream arq;
    nomeArq = PASTA_DESEMPENHO + nomeArq;
    cout <<nomeArq << endl;
    arq.open(nomeArq.c_str());
    cout << arq.good();
    arq << "Problema: " << problema->getNome() << endl;
    arq << "Medias para amostra = " << amostra;
    arq << "Algoritmo;Estados Expandidos;Estados Visitados;Fator Ramificacao;Profundidade da Solucao;Custo da Solucao;Clocks\n";
    for(uint i = BACKTRACKING; i <= IDA_ESTRELA; i++){
        arq << busca->nomeAlgoritmo(medias[i]->algoritmo) << ";"
            << medias[i]->qtdEstadosExpandidos  / amostra          << ";"
            << medias[i]->qtdEstadosVisitados   / amostra          << ";"
            << medias[i]->fatorRamificacao      / amostra          << ";"
            << medias[i]->profundidade          / amostra          << ";"
            << medias[i]->custoDaSolucao        / amostra          << ";"
            << medias[i]->clocks                / amostra           << endl;
    }
    arq << "\nCLOCKS_PER_SECOND\n" << CLOCKS_PER_SEC << endl;
    arq.close();

    for (int i = BACKTRACKING; i <= IDA_ESTRELA; ++i)
        delete medias[i];

    cout << "salvo" << endl;
    delete problema;
    delete busca;
}

int main() {
    srand((uint) 1);
//    testeSequencia(PILHA);
//    testeSequencia(FILA);
//    testeSequencia(LISTA);

    int metodo = NRAINHAS2;

//    uint min = 6, max = 10;
//    testeBuscaNRainhas(metodo, BACKTRACKING, min, max);
//    testeBuscaNRainhas(metodo, PROFUNDIDADE, min, max);
//    testeBuscaNRainhas(metodo, LARGURA, min, max);
//    testeBuscaNRainhas(metodo, ORDENADA, min, max);
//    testeBuscaNRainhas(metodo, GULOSA, min, max);
//    testeBuscaNRainhas(metodo, A_ESTRELA, min, max);
//    testeBuscaNRainhas(metodo, IDA_ESTRELA, min, max);

    for(uint i = 1; i <=10; i++){
        if(metodo == NRAINHAS1)
            auxTesteDesempenho(new NRainhas1(i), 5);
        if(metodo == NRAINHAS2)
            auxTesteDesempenho(new NRainhas2(i), 5);
    }


//    testeOperacoesMontes(4);
//    testeMontesIguais(BACKTRACKING);
//    testeMontesIguais(PROFUNDIDADE);
//    testeMontesIguais(LARGURA);

    return 0;
}
