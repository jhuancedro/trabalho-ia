//
// Created by jhcedro on 08/05/17.
//
#include "Tabuleiro.h"
using namespace std;

/* Cria um tabuleiro quadrado (n x n), n > 0 */
Tabuleiro::Tabuleiro(uint n, uint profundidade, bool permitirColisoes) : Estado(profundidade){
    this->n = n;
    linhas = new uint[n];
    colunas = new uint[n];
    diagonais1 = new uint[2*n-1];
    diagonais2 = new uint[2*n-1];

    for (uint i = 0; i < n; ++i)
        linhas[i] = colunas[i] = 0;

    for (uint j = 0; j < 2*n-1; ++j)
        diagonais1[j] = diagonais2[j] = 0;

    posicoes = new Pilha<Posicao*>();
    numRainhas = 0;
    setCusto(0);

    this->permitirColisoes = permitirColisoes;
}

uint Tabuleiro::getTamTabuleiro() {
    return n;
}

uint Tabuleiro::getNumRainhas(){
    return numRainhas;
}

/* Mapeia posiçao (i, j) em um vetor de diagonais: '\'
 * 2   3   4
 * 1   2   3
 * 0   1   2
 * */
uint Tabuleiro::posDiagonal1(uint i, uint j){
    return j + (n-1)-i;
}

/* Mapeia posiçao (i, j) em um vetor de diagonais: '/'
 * 0   1   2
 * 1   2   3
 * 2   3   4
 * */
uint Tabuleiro::posDiagonal2(uint i, uint j){
    return i+j;
}

bool Tabuleiro::posicaoVazia(uint i, uint j){
    return linhas[i] == 0 &&
           colunas[j] == 0 &&
           diagonais1[posDiagonal1(i, j)] == 0 &&
           diagonais2[posDiagonal2(i, j)] == 0;
}

bool Tabuleiro::posicaoDisponivel(uint i, uint j) {
    if (i < 0 || i >= n || j < 0 || j >= n) return false;
    if (permitirColisoes)
        return !possuiRainha(i, j);
    else
        posicaoVazia(i, j);
}

bool Tabuleiro::linhaOcupada(uint i) {
    return linhas[i] != 0;
}

void Tabuleiro::ocuparPosicao(uint i, uint j){
    linhas[i]++;
    colunas[j]++;
    diagonais1[posDiagonal1(i, j)]++;
    diagonais2[posDiagonal2(i, j)]++;
}

void Tabuleiro::liberarPosicao(uint i, uint j) {
    /*
    linhas[i] = VAZIA;
    for (int k = 0; k < n; ++k) {
        if (possuiRainha(i, k)){
            linhas[i] = OCUPADA;
            break;
        }
    }

    colunas[j] = VAZIA;
    for (int k = 0; k < n; ++k) {
        if (possuiRainha(k, j)){
            colunas[i] = OCUPADA;
            break;
        }
    }

    diagonais1[i] = VAZIA;
    for (int k = 0; k < n; ++k) {
        if (i+k-j > 0 && i+k-j < n && possuiRainha(i+k-j, k)){
            diagonais1[i] = OCUPADA;
            break;
        }
    }

    diagonais2[i] = VAZIA;
    for (int k = 0; k < n; ++k) {
        if (k-i-j > 0 && k-i-j < n && possuiRainha(k-i-j, k)){
            diagonais2[i] = OCUPADA;
            break;
        }
    }*/

    linhas[i]--;
    colunas[j]--;
    diagonais1[posDiagonal1(i, j)]--;
    diagonais2[posDiagonal2(i, j)]--;

/*    linhas[i] = colunas[i] = diagonais1[posDiagonal1(i, j)] = diagonais2[posDiagonal2(i, j)] = VAZIA;
    for (posicoes->itInicio(); !posicoes->itEhFim(); posicoes->itProx()) {


        Posicao *p = posicoes->getIt();
        if (p->i == i)  linhas[i] = OCUPADA;
        if (p->j == j)  colunas[j] = OCUPADA;
        if (posDiagonal1(p->i, p->j) == posDiagonal1(i, j)) diagonais1[posDiagonal1(i, j)] = OCUPADA;
        if (posDiagonal2(p->i, p->j) == posDiagonal2(i, j)) diagonais2[posDiagonal2(i, j)] = OCUPADA;

        if(linhas[i] == OCUPADA
           && colunas[i] == OCUPADA
           && diagonais1[posDiagonal1(i, j)] == OCUPADA
           && diagonais2[posDiagonal2(i, j)] == OCUPADA)
            break;
    }
    */
}

/* Insere rainha na posicao (i, j) e indisponibiliza posicoes relacionadas*/
bool Tabuleiro::inserirRainha(uint i, uint j){
    if (!posicaoDisponivel(i, j)) return false;
    ocuparPosicao(i, j);

    Posicao* p = new Posicao;
    p->i = i;
    p->j = j;
    posicoes->inserir(p);
    numRainhas++;

    return true;
}

bool Tabuleiro::moverRainha(uint rainha, uint direcao) {
    if (rainha > numRainhas)    return false;

    // direcoes para mover a rainha
    // 0 | 1 | 2
    // 3 | R | 4
    // 5 | 6 | 7
    if (direcao > 3) direcao++;
    uint i = direcao / 3;
    uint j = direcao % 3;


    uint k = 0;
    for(posicoes->itInicio(); !posicoes->itEhFim(); posicoes->itProx(), k++){
        if (k == rainha){
            Posicao* p = posicoes->getIt();
            uint novoI = p->i + i - 1, novoJ = p->j + j - 1;

            if(novoI < 0 || novoI >= n || novoJ < 0 || novoJ >= n || !posicaoDisponivel(novoI, novoJ))  return false;
//            printf("Movendo Rainha (%d, %d) para a direcao (%d, %d) \n", p->i, p->j, novoI, novoJ);

            liberarPosicao(p->i, p->j);
            ocuparPosicao(novoI, novoJ);
            p->i = novoI;
            p->j = novoJ;

            return true;
        }
    }
    return false;
}

bool Tabuleiro::possivelMoverRainha(uint rainha, uint direcao) {
    if (rainha > numRainhas)    return false;

    // direcoes para mover a rainha
    // 0 | 1 | 2
    // 3 | R | 4
    // 5 | 6 | 7
    if (direcao > 3) direcao++;
    uint i = direcao / 3;
    uint j = direcao % 3;

    uint k = 0;
    for(posicoes->itInicio(); !posicoes->itEhFim(); posicoes->itProx(), k++){
        if (k == rainha){
            Posicao* p = posicoes->getIt();
            uint novoI = p->i + i - 1, novoJ = p->j + j - 1;

            return !(novoI < 0 || novoI >= n || novoJ < 0 || novoJ >= n || !posicaoDisponivel(novoI, novoJ));
        }
    }
    return false;
}

/* Verifica quantas colisoes existem em quaisquer pares de rainhas*/
uint Tabuleiro::contarColisoes() {
    uint cont = 0;
    for (uint i = 0; i < n; ++i) {
        cont += linhas[i]*(linhas[i]-1)/2;
        cont += colunas[i]*(colunas[i]-1)/2;
    }
    for (uint i = 0; i < 2*n-1; ++i) {
        cont += diagonais1[i]*(diagonais1[i]-1)/2;
        cont += diagonais2[i]*(diagonais2[i]-1)/2;
    }

    return cont;
}

/* Verifica ha colisao entre quaisquer pares de rainhas*/
bool Tabuleiro::haColisao() {
    uint cont = 0;
    for (uint i = 0; i < n; ++i)
        if(linhas[i] > 1 || colunas[i] > 1) return true;

    for (uint i = 0; i < 2*n-1; ++i)
        if(diagonais1[i] > 1 || diagonais2[i] > 1) return true;

    /*
    for(posicoes->itInicio(); !posicoes->itEhFim(); posicoes->itProx()){
        Posicao* p = posicoes->getIt();
        for (int k = p->i+1; k < n; ++k) {
            if (p->i != k && possuiRainha(k, p->j)) return true;
        }
        for (int k = p->j+1; k < n; ++k) {
            if (p->j != k && possuiRainha(p->i, k)) return true;
        }
        for (int k = 1; k < n-p->j; ++k) {
            uint l = p->i - k;   // percorre diagonal '\'
            if (l >= 0 && l < n && l != p->i && possuiRainha(l, k)) return true;

            uint m = p->i + k;   // percorre diagonal '/'
            if (m >= 0 && m < n && m != p->i  && possuiRainha(m, k)) return true;
        }
    }*/
    return false;
}

/* Verifica se ha uma rainha na posicao (i, j) */
bool Tabuleiro::possuiRainha(uint i, uint j) {
    bool posOcupada = false;
    posicoes->pushIt();
    for (posicoes->itInicio(); !posOcupada && !posicoes->itEhFim(); posicoes->itProx()) {
        Posicao *p = posicoes->getIt();
        posOcupada = posOcupada || (p->i == i && p->j == j);
    }
    posicoes->popIt();
    return posOcupada;
}

Tabuleiro* Tabuleiro::tabuleiroAleatorio(uint n) {
    Tabuleiro* T = new Tabuleiro(n, 0);

    // Preenche com n Rainhas em posicoes aleatorias
    for (int k = 0; k < n; ++k) {
        uint i, j;
        do{
            i = rand() % n;
            j = rand() % n;
        }while (!T->posicaoDisponivel(i, j));

        T->inserirRainha(i, j);
//        imprime();
    }

    return T;
}

/* Imprime um vetor de sint*/
void Tabuleiro::imprimeVetor(uint* vetor, uint tam){
    cout << "[";
    for (int i = 0; i < tam; i++)
        printf("%d %s", vetor[i], (i != tam-1 ? ", " : ""));
    cout << "]" << endl;
}

/** Imprime informacoes da estrutura do tabuleiro*/
void Tabuleiro::imprimeInfo(){
    cout << "linhas: "; imprimeVetor(linhas, n);
    cout << "colunas: "; imprimeVetor(colunas, n);
    cout << "diagonais1: "; imprimeVetor(diagonais1, 2*n-1);
    cout << "diagonais2: "; imprimeVetor(diagonais2, 2*n-1);

    cout << "Pilha de posições: ";
    cout << "{ ";
    for (posicoes->itInicio(); !posicoes->itEhFim(); posicoes->itProx()) {
        Posicao* no = posicoes->getIt();
        printf("(%d, %d), ", no->i, no->j);
    }
    cout << "}" << endl;
}

/*  Imprime o tabuleiro
 *  A impressão não eh eficiente, pois nao eh apropriada aa estrutura adotada*/
void Tabuleiro::imprime(){
    string aux = "";
    for (int k = 0; k < n; ++k)
        aux += "|---";
    aux += "|";

    cout << aux << endl;
    for (uint i = 0; i < n; ++i) {
        cout << "| ";
        for (uint j = 0; j < n; ++j) {
            if (!posicaoVazia(i, j)) {
                if (possuiRainha(i, j))
                    cout << 'R' << " | ";
                else
                    cout << 'x' << " | ";
            } else
                cout << ' ' << " | ";
        }
        cout << endl << aux << endl;
    }
    cout << endl;
}

Tabuleiro* Tabuleiro::copia(uint profundidade){
    Tabuleiro* tabuleiro = new Tabuleiro(n, profundidade);

    if(this->numRainhas > 0) {
        Posicao* aux_posicoes[this->numRainhas];
        uint k = 0;
        for (posicoes->itInicio(); !posicoes->itEhFim(); posicoes->itProx())
            aux_posicoes[k++] = posicoes->getIt();

        for (k = 0; k < this->numRainhas; k++)
            tabuleiro->inserirRainha(aux_posicoes[numRainhas-1-k]->i,
                                     aux_posicoes[numRainhas-1-k]->j);
    }

    tabuleiro->setCusto(this->getCusto());

    return tabuleiro;

    /*for(int i = 0; i < 2*n - 1; i++){
        if(i < n){
            tabuleiro->linhas[i] = this->linhas[i];
            tabuleiro->colunas[i] = this->colunas[i];
        }
        tabuleiro->diagonais1[i] = this->diagonais1[i];
        tabuleiro->diagonais2[i] = this->diagonais2[i];
    }*/
}

uint Tabuleiro::qdtPosicoesIndisponiveis(){
    uint cont = 0;
    for(uint i = 0; i < n; i++){
        if(linhas[i] != OCUPADA){
            for(uint j = 0; j < n; j++){
                if(posicaoVazia(i, j))
                    cont++;
            }
        }
    }
    return n*n - cont;
}

Tabuleiro::~Tabuleiro(){
    delete [] linhas;
    delete [] colunas;
    delete [] diagonais1;
    delete [] diagonais2;

    // deleta posicoes alocadas
    for(posicoes->itInicio(); !posicoes->itEhFim(); posicoes->itProx()){
        delete posicoes->getIt();
    }

    delete posicoes;
}
